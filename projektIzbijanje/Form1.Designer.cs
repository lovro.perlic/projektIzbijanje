﻿namespace projektIzbijanje
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            txtScore = new Label();
            player = new PictureBox();
            ball = new PictureBox();
            gameTImer = new System.Windows.Forms.Timer(components);
            pickupDurationLabel = new Label();
            ((System.ComponentModel.ISupportInitialize)player).BeginInit();
            ((System.ComponentModel.ISupportInitialize)ball).BeginInit();
            SuspendLayout();
            // 
            // txtScore
            // 
            txtScore.AutoSize = true;
            txtScore.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            txtScore.ForeColor = Color.White;
            txtScore.Location = new Point(17, 20);
            txtScore.Margin = new Padding(4, 0, 4, 0);
            txtScore.Name = "txtScore";
            txtScore.Size = new Size(122, 29);
            txtScore.TabIndex = 0;
            txtScore.Text = "Bodovi: 0";
            // 
            // player
            // 
            player.BackColor = Color.White;
            player.Location = new Point(463, 757);
            player.Margin = new Padding(4, 5, 4, 5);
            player.Name = "player";
            player.Size = new Size(133, 49);
            player.TabIndex = 1;
            player.TabStop = false;
            // 
            // ball
            // 
            ball.Anchor = AnchorStyles.None;
            ball.BackColor = Color.Transparent;
            ball.BackgroundImageLayout = ImageLayout.Center;
            ball.Image = (Image)resources.GetObject("ball.Image");
            ball.Location = new Point(503, 457);
            ball.Margin = new Padding(4, 5, 4, 5);
            ball.Name = "ball";
            ball.Size = new Size(32, 32);
            ball.SizeMode = PictureBoxSizeMode.AutoSize;
            ball.TabIndex = 1;
            ball.TabStop = false;
            ball.Click += ball_Click;
            // 
            // gameTImer
            // 
            gameTImer.Interval = 20;
            gameTImer.Tick += mainGameTimerEvent;
            // 
            // pickupDurationLabel
            // 
            pickupDurationLabel.AutoSize = true;
            pickupDurationLabel.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            pickupDurationLabel.ForeColor = Color.White;
            pickupDurationLabel.Location = new Point(503, 20);
            pickupDurationLabel.Margin = new Padding(4, 0, 4, 0);
            pickupDurationLabel.Name = "pickupDurationLabel";
            pickupDurationLabel.Size = new Size(201, 29);
            pickupDurationLabel.TabIndex = 2;
            pickupDurationLabel.Text = "Vrijeme pickupa";
            pickupDurationLabel.Click += label1_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            AutoValidate = AutoValidate.Disable;
            BackColor = Color.Black;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Zoom;
            ClientSize = new Size(951, 825);
            Controls.Add(pickupDurationLabel);
            Controls.Add(ball);
            Controls.Add(player);
            Controls.Add(txtScore);
            DoubleBuffered = true;
            ForeColor = SystemColors.ActiveCaption;
            Margin = new Padding(4, 5, 4, 5);
            MaximizeBox = false;
            Name = "Form1";
            Text = "Izbijanje";
            Load += Form1_Load;
            KeyDown += keyisdown;
            KeyUp += keyisup;
            ((System.ComponentModel.ISupportInitialize)player).EndInit();
            ((System.ComponentModel.ISupportInitialize)ball).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label txtScore;
        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.Timer gameTImer;
        private Label pickupDurationLabel;
    }
}