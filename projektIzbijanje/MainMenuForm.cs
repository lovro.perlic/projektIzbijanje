﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System;
using System.Windows.Forms;

namespace projektIzbijanje
{
    public partial class MainMenuForm : Form
    {
        private Form1 igra;
        public MainMenuForm()
        {
            InitializeComponent();
        }

     

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 igra = new Form1();
            igra.FormClosed += IgraForm_FormClosed; // Pretplata na događaj zatvaranja Form1
            
            
            igra.Show();
            this.Hide(); // Skrivanje glavnog menija
        }

        private void IgraForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            igra = null; // Oslobađanje reference na Form1
            this.Show(); // Ponovno prikazivanje glavnog menija
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
