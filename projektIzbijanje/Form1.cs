using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace projektIzbijanje
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.Timer shiftKeyPressTimer = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tensec = new System.Windows.Forms.Timer();
        bool goLeft;
        bool goRight;
        bool isGameOver;
        bool isShiftKeyPressed = true;
        int score;
        int ballx;
        int bally;
        int playerSpeed;
        bool slowdownColActive = false;
        bool speedupActive = false;
        bool speedupColActive = false;
        private int speedupTimeLeft = 0;
        private int defaultPlayerSpeed = 12;
        private int pickupDuration = 10;
        private int pickupSpeed = 8;
        int slowdownTime = 10;
        int slowdownTimeLeft = 0;
        Random rnd = new Random();

        PictureBox[] blockArray;

        public Form1()
        {
            InitializeComponent();
            AddWalls();
            PlaceBlocks();
        }

        private void setupGame()
        {
            isGameOver = false;
            score = 0;
            ballx = 5;
            bally = 5;
            playerSpeed = 12;
            txtScore.Text = "Bodovi: " + score;

            ball.Left = 376;
            ball.Top = 328;

            player.Left = 347;
            pickupDurationLabel.Text = "Trajanje pickup-a:" + (speedupTimeLeft) + "  sekundi";
            pickupDurationLabel.Visible = false;
            shiftKeyPressTimer.Interval = 1000;
            shiftKeyPressTimer.Tick += (sender, e) =>
            {

                speedupTimeLeft -= 1;


            };

            tensec.Interval = 1000;
            tensec.Tick += (sender, e) =>
            {

                slowdownTimeLeft -= 1;


            };

            gameTImer.Start();

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "blocks")
                {
                    x.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                   // x.BorderStyle = BorderStyle.FixedSingle;
                    GraphicsPath gp = new GraphicsPath();
                    gp.AddEllipse(0, 0, x.Width, x.Height);
                    Region region = new Region(gp);
                    x.Region = region;

                   /* // Dodavanje gradient boje
                    LinearGradientBrush brush = new LinearGradientBrush(x.ClientRectangle, Color.Red, Color.Yellow, LinearGradientMode.Vertical);
                    Bitmap bmp = new Bitmap(x.Width, x.Height);
                    Graphics g = Graphics.FromImage(bmp);
                    g.FillEllipse(brush, 0, 0, x.Width, x.Height);
                    x.BackgroundImage = bmp;
                   */
                    // Dodavanje obruba
                    x.Padding = new Padding(3); // Veli�ina obruba
                    x.Margin = new Padding(-3); // Negativni margini kako bi se obrub prikazao unutar PictureBox-a
                    x.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)); // Boja obruba
                }
            }
        }


        private void gameOver(string message)
        {
            isGameOver = true;
            gameTImer.Stop();

            txtScore.Text = "Bodovi: " + score + " " + message;



        }


        private void AddWalls()
        {
            // Dodavanje lijevog zida
            PictureBox leftWall = new PictureBox();
            leftWall.BackColor = Color.Gray;
            leftWall.Size = new Size(10, this.ClientSize.Height); // �irina i visina
            leftWall.Location = new Point(0, 0); // Pozicija
            this.Controls.Add(leftWall);

            // Dodavanje desnog zida
            PictureBox rightWall = new PictureBox();
            rightWall.BackColor = Color.Gray;
            rightWall.Size = new Size(10, this.ClientSize.Height); // �irina i visina
            rightWall.Location = new Point(820, 0); // Pozicija
            this.Controls.Add(rightWall);

            // Dodavanje gornjeg zida
            PictureBox topWall = new PictureBox();
            topWall.BackColor = Color.Gray;
            topWall.Size = new Size(this.ClientSize.Width, 10); // �irina i visina
            topWall.Location = new Point(0, 0); // Pozicija
            this.Controls.Add(topWall);

            // Dodavanje donjeg zida
            PictureBox bottomWall = new PictureBox();
            bottomWall.BackColor = Color.Gray;
            bottomWall.Size = new Size(this.ClientSize.Width, 10); // �irina i visina
            bottomWall.Location = new Point(0, this.ClientSize.Height - 10); // Pozicija
            this.Controls.Add(bottomWall);
        }


        private void PlaceBlocks()

        {
            blockArray = new PictureBox[15];

            int a = 0;

            int top = 50;
            int left = 100;

            for (int i = 0; i < blockArray.Length; i++)
            {
                blockArray[i] = new PictureBox();
                blockArray[i].Height = 32;
                blockArray[i].Width = 100;
                blockArray[i].Tag = "blocks";
                blockArray[i].BackColor = Color.White;


                if (a == 5)
                {
                    top = top + 50;
                    left = 100;
                    a = 0;
                }

                if (a < 5)
                {
                    a++;
                    blockArray[i].Left = left;
                    blockArray[i].Top = top;
                    this.Controls.Add(blockArray[i]);
                    left = left + 130;
                }

            }
            setupGame();
        }




        private void removeBlocks()
        {
            foreach (PictureBox x in blockArray)
            {
                this.Controls.Remove(x);
            }
        }



        private async void mainGameTimerEvent(object sender, EventArgs e)
        {
            txtScore.Text = "Bodovi: " + score;

            if (goLeft == true && player.Left > 0)
            {

                player.Left -= playerSpeed;
            }

            if (goRight == true && player.Left < 700)
            {
                player.Left += playerSpeed;
            }

            ball.Left += ballx;
            ball.Top += bally;

            if (ball.Left < 0 || ball.Left > 775)
            {
                ballx = -ballx;
            }
            if (ball.Top < 0)
            {
                bally = -bally;
            }

            if (ball.Bounds.IntersectsWith(player.Bounds))
            {

                ball.Top = 463;

                bally = rnd.Next(5, 12) * -1;

                if (ballx < 0)
                {
                    ballx = rnd.Next(5, 12) * -1;
                }
                else
                {
                    ballx = rnd.Next(5, 12);
                }
            }

            int totalPickupsGenerated = 0;

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "blocks")
                {
                    if (ball.Bounds.IntersectsWith(x.Bounds))
                    {
                        score += 1;
                        bally = -bally;
                        this.Controls.Remove(x);

                        if (ShouldGeneratePickup(totalPickupsGenerated))
                        {
                            GeneratePickupBomb(x.Left + x.Width / 2, x.Top + x.Height / 2);
                            totalPickupsGenerated++;
                        }
                        if (ShouldGeneratePickup(totalPickupsGenerated))
                        {
                            GeneratePickupSlow(x.Left + x.Width / 2, x.Top + x.Height / 2);
                            totalPickupsGenerated++;
                        }
                        if (ShouldGeneratePickup(totalPickupsGenerated))
                        {
                            GeneratePickup(x.Left + x.Width / 2, x.Top + x.Height / 2);
                            totalPickupsGenerated++;
                        }
                    }
                }
            }

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickup")
                {
                    x.Top += pickupSpeed;
                }

            }

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickupslow")
                {
                    x.Top += pickupSpeed;
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {

                        this.Controls.Remove(x);
                        slowdownColActive = true;
                        slowdownTimeLeft = slowdownTime;


                    }
                }
            }


            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickupbomb")
                {
                    x.Top += pickupSpeed;
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {

                        this.Controls.Remove(x);
                        gameOver("Izgubio si!! Lupi Enter za opet");


                    }
                }
            }




            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickup")
                {
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {

                        this.Controls.Remove(x);
                        speedupColActive = true;
                        speedupTimeLeft = pickupDuration;

                    }
                }
            }

            if (!speedupColActive)
            {
                pickupDurationLabel.Visible = false;
            }

            if (score == 15)
            {
                gameOver("Pobjeda!! Lupi Enter za opet");
            }

            if (ball.Top > 580)
            {
                gameOver("Izgubio si!! Lupi Enter za opet");
            }





            if (speedupTimeLeft <= 0)
            {
                speedupTimeLeft = 0;
                speedupActive = false;
                speedupColActive = false;
                pickupDurationLabel.Visible = false;
                playerSpeed = defaultPlayerSpeed;
            }







            if (slowdownColActive)
            {



                tensec.Start();
                pickupDurationLabel.Visible = true;
                pickupDurationLabel.Text = "Trajanje pickup-a:" + (slowdownTimeLeft) + "  sekundi";
                playerSpeed = 5;
                if (slowdownTimeLeft <= 0)
                {
                    tensec.Stop();
                    playerSpeed = defaultPlayerSpeed;
                    pickupDurationLabel.Visible = false;
                    slowdownColActive = false;

                }

            }





        }
        private void GeneratePickupSlow(int x, int y)
        {
            PictureBox pickupslow = new PictureBox();
            pickupslow.BackColor = Color.Transparent;
            pickupslow.Image = Image.FromFile(@"pill.png");
            pickupslow.Size = new Size(50, 50);
            pickupslow.Location = new Point(x, y);
            pickupslow.Tag = "pickupslow";
            this.Controls.Add(pickupslow);
        }


        private void GeneratePickupBomb(int x, int y)
        {
            PictureBox pickupbomb = new PictureBox();
            pickupbomb.BackColor = Color.Transparent;
            pickupbomb.Image = Image.FromFile(@"bomb.png") ;
            pickupbomb.Size = new Size(50, 50);
            pickupbomb.Location = new Point(x, y);
            pickupbomb.Tag = "pickupbomb";
            this.Controls.Add(pickupbomb);
        }



        private void GeneratePickup(int x, int y)
        {
            PictureBox pickup = new PictureBox();
            pickup.BackColor = Color.Transparent;
            pickup.Image = Image.FromFile(@"light.png"); 
            pickup.Size = new Size(50, 50);
            pickup.Location = new Point(x, y);
            pickup.Tag = "pickup";
            this.Controls.Add(pickup);
        }
        private void RemovePickups()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickup")
                {
                    this.Controls.Remove(x);
                }
            }
        }

        private void RemovePickupsSlow()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickupslow")
                {
                    this.Controls.Remove(x);
                }
            }
        }

        private void RemovePickupsBomb()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "pickupbomb")
                {
                    this.Controls.Remove(x);
                }
            }
        }

        private bool ShouldGeneratePickup(int totalPickupsGenerated)
        {
            int totalPickups = 3;
            int minPickups = 2;
            int remainingPickups = totalPickups - totalPickupsGenerated;

            if (remainingPickups <= (totalPickups - minPickups))
            {
                return true;
            }
            else
            {
                int probability = 15;
                int randomNumber = rnd.Next(1, probability + 1);
                return randomNumber == 1;
            }
        }



        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                goLeft = true;
            }
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                goRight = true;
            }
            if (e.KeyCode == Keys.ShiftKey && speedupTimeLeft > 0 && speedupColActive && !isShiftKeyPressed)
            {
                shiftKeyPressTimer.Start();
                isShiftKeyPressed = true;
                speedupActive = true;
                playerSpeed = 50;





            }
        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && isGameOver)
            {
                slowdownColActive = false;
                slowdownTimeLeft = 0;
                speedupActive = false;
                speedupActive = false;
                speedupTimeLeft = 0;
                removeBlocks();
                RemovePickups();
                RemovePickupsSlow();
                RemovePickupsBomb();
                PlaceBlocks();


            }
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                goLeft = false;
            }
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                goRight = false;
            }
            if (e.KeyCode == Keys.ShiftKey && isShiftKeyPressed)
            {
                shiftKeyPressTimer.Stop();
                pickupDurationLabel.Text = "Trajanje pickup-a: " + speedupTimeLeft + " sekundi";
                pickupDurationLabel.Visible = true;
                speedupActive = false;
                playerSpeed = defaultPlayerSpeed;
                isShiftKeyPressed = false;

            }
        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ball_Click(object sender, EventArgs e)
        {

        }
    }
}

